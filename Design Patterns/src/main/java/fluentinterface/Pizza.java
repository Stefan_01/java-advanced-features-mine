package fluentinterface;

public interface Pizza {
    public Pizza getName();
    public Pizza getIngredients();
    public Pizza getCost();


}

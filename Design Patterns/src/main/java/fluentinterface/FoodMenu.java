package fluentinterface;

import java.util.ArrayList;
import java.util.List;

public class FoodMenu implements Menu {
    List<Pizza> pizzaList;
    List<String> selectedPizza;
    public FoodMenu() {
        this.pizzaList = new ArrayList<>();
        populateMenu();
        this.selectedPizza = new ArrayList<>();
    }

    private void populateMenu() {
       Pizza pizza = new PizzaQuatroSagioni();
       pizzaList.add(pizza);

    }

    public Menu orderPizza(List<String> orders){
        for(String order:orders){
        selectedPizza.add(order);
            System.out.println("I have orderd pizza" + " " + order);
        }

        return this;

    }
    public Menu eatPizza(){
        System.out.println("I have eaten pizza");
        return this;
    }
    public Menu payPizza(){
        System.out.println("I payed for the pizza");
        return this;
    }

}

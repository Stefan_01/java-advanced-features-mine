package Inheritance;

public class Scissers extends Tool {
    private String color;

    public Scissers(){
        super();
        this.color = "";
    }

    public Scissers(String name, String scope, String price) {
        super(name, scope, price);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Scissors{" +
                "color='" + color + '\'' +
                ", name='" + name + '\'' +
                ", scope='" + scope + '\'' +
                ", price='" + price + '\'' +
                '}';

    }

}

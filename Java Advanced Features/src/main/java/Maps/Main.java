package Maps;

import exercitii.Person;
import exercitii.Staff;
import exercitii.Student;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, Person> persons = new HashMap<>();
        Person man1 = new Student("info",2000,5000, "Adrian","Cluj,Romania");
        Person man2 = new Staff("info",3000);
        persons.put("person1",man1);
        persons.put("person2", man2);
        for(Map.Entry<String,Person> person: persons.entrySet()) {
            String key = person.getKey();
            Person value = person.getValue();
            System.out.println( key + " " + value);
        }

    }
}

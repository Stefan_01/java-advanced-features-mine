package exercitii;

public class Main {
    public static void main(String[] args) {
        Person man1 = new Student("info",2000,5000, "Adrian","Cluj,Romania");
        Person man2 = new Staff("info",3000);
        man2.setName("paul");
        man2.setAdress("Timisoara");
        System.out.println(man1);
        System.out.println(man2);

        printNameAddress(man1);
        printNameAddress(man2);

        man1.f();
        man2.f();

    }
    public static void printNameAddress(Person person){
        System.out.println(person.getName() + " - " + person.getAdress());
    }
}

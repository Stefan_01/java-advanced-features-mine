package exercitii;

public abstract class  Person {
   protected String name;
   protected String address;

   public abstract void f();

    public Person(){
        this.name = "";
        this.address = "";
    }

    public Person(String name, String adress) {
        this.name = name;
        this.address = adress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return address;
    }

    public void setAdress(String adress) {
        this.address = adress;
    }

    @Override
    public String toString() {
        return String.format("%s->%s", name, address);
    }
}

package exercitii;

public class Student extends Person{
    private String study;
    private int year;
    private int studyPrice;

    @Override
    public void f() {
        System.out.println("Sunt studentul " + name);
    }

    public Student(String study, int year, int studyPrice, String name, String address) {
        super(name,address);
        this.study = study;
        this.year = year;
        this.studyPrice = studyPrice;
    }

    public Student(String study, int year, int studyPrice) {
        super();
        this.study = study;
        this.year = year;
        this.studyPrice = studyPrice;
    }

    public String getStudy() {
        return study;
    }

    public void setStudy(String study) {
        this.study = study;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getStudyPrice() {
        return studyPrice;
    }

    public void setStudyPrice(int studyPrice) {
        this.studyPrice = studyPrice;
    }

    @Override
    public String toString() {
        return "Student{" +
                "study='" + study + '\'' +
                ", year=" + year +
                ", studyPrice=" + studyPrice +
                ", name='" + name + '\'' +
                ", adress='" + address + '\'' +
                '}';
    }
}
